from string import ascii_lowercase


def filter_all(*args):
    """Return characters that appear in each arg."""
    return set(args[0]).intersection(*args[1:])


def filter_ge_one(*args):
    """Return characters that appear in at least one arg."""

    # return set().union(*args)
    return set(''.join(args))


def filter_ge_two(*args):
    """Return characters that appear at least in two args."""

    lists = [list(set(arg)) for arg in args]
    union = [char for lst in lists for char in lst]

    return {char for char in union if union.count(char) >= 2}


def filter_other(*args):
    """Return characters that were not used in any arg."""

    return set(ascii_lowercase) - filter_ge_one(*args)


if __name__ == '__main__':
    strings = [
        'hello',
        'world',
        'python',
    ]
    print('Strings:', strings)
    print('Characters that appear in all strings:', filter_all(*strings))
    print('Characters that appear in at least one string:',
          filter_ge_one(*strings))
    print('Characters that appear at least in two string:',
          filter_ge_two(*strings))
    print('Characters of alphabet, that were not used in any string:',
          filter_other(*strings))
