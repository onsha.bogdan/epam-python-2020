def is_palindrome(s: str) -> bool:
    s = ''.join([ch for ch in s if ch.isalnum()]).lower()
    return s == s[::-1]


if __name__ == '__main__':
    cases = [
        'Mom',
        'Uncle',
        'Do geese see God?',
        'Punctuation does not matter.',
        'Madam, I\'m Adam.',
        '353',
        '123'
    ]

    for case in cases:
        print(f'String "{case}" is a palindrome:', is_palindrome(case))
