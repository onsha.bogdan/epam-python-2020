# Task 9
  
## Exercise 1.  
Implement a function which receives a string and replaces all `'` symbols with `"` and vise versa.

### Description
Regular solution:
```py 
def swap_quotes(s: str) -> str:
    swapped = []
    for char in s:
        if char == '"':
            swapped.append("'")
        elif char == "'":
            swapped.append('"')
        else:
            swapped.append(char)
    return ''.join(swapped)
```
One-liner:
```py 
def swap_quotes(s: str) -> str:
    return ''.join(["'" if i == '"' else '"' if i == "'" else i for i in s])
```

`exercise_1.py` output:

![Task Description](https://i.ibb.co/0VzhN6Z/Screenshot-2020-11-04-at-14-07-57.png)  

[Code](./exercise_1.py)


## Exercise 2.  
Write a function that check whether a string is a palindrome or not.

### Description

Solution:
- remove all non-alphanumeric characters from string.
- lower the string.
- compare the string with itself reversed.

```py 
def is_palindrome(s: str) -> bool:
    s = ''.join([ch for ch in s if ch.isalnum()]).lower()
    return s == s[::-1]
```

`exercise_2.py` output:

![Task Description](https://i.ibb.co/02KQpnQ/Screenshot-2020-11-04-at-14-10-22.png)  

[Code](./exercise_2.py)


## Exercise 3.  
Implement a function ‘get_shortest_word(s: str) -> str’
which returns the shortest word in the given string. The
word can contain any symbols except whitespaces (" ,
‘\n’, ‘\t and so on). If there are multiple shortest words
in the string with a same length return the word that
occurs first. Usage of any split functions is forbidden.

Solution:
```py 
def get_shortest_word(s: str):
    result = list(s)
    temp = []
    for char in s:
        if char.isspace():
            if len(temp) < len(result) and len(temp) != 0:
                result = temp[::]
            temp.clear()
        else:
            temp.append(char)
    return ''.join(result)
```

`exercise_3.py` output:

![Task Description](https://i.ibb.co/Cmhy2xf/Screenshot-2020-11-04-at-14-15-07.png)  

[Code](./exercise_3.py)

## Exercise 4.  
Implement a bunch of functions which receive a
changeable number of strings and return next
parameters:
- characters that appear in all strings
- characters that appear in at least one string
- characters that appear at least in two strings
- characters of alphabet, that were not used in any
string
Note: use ‘string.ascii_lowercase’ for list of alphabet
letters

`exercise_4.py` output:

![Task Description](https://i.ibb.co/qJKxT5Y/Screenshot-2020-11-04-at-14-18-08.png)  

[Code](./exercise_4.py)



## Exercise 5.  
Implement a function, that takes string as an argument and returns a dictionary, that contains letters of given string as
keys and a number of their occurrence as values.

Solution:
```py 
def count_letters(s: str) -> dict:
    return {char: s.count(char) for char in set(s)}
```


`exercise_5.py` output:

![Task Description](https://i.ibb.co/zGK0gCn/Screenshot-2020-11-04-at-14-24-14.png)  

[Code](./exercise_5.py)


