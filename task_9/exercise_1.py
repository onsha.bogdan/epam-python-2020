def swap_quotes(s: str) -> str:
    swapped = []
    for char in s:
        if char == '"':
            swapped.append("'")
        elif char == "'":
            swapped.append('"')
        else:
            swapped.append(char)
    return ''.join(swapped)


def swap_quotes_one_liner(s: str) -> str:
    return ''.join(["'" if i == '"' else '"' if i == "'" else i for i in s])


if __name__ == '__main__':
    string = """String with some quotes - ' " ' and " ' "."""
    print(f'String:', string)
    print('Swapped string:', swap_quotes(string))
    print('Swapped string (one-liner):', swap_quotes_one_liner(string))
