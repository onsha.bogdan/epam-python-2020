def count_letters(s: str) -> dict:
    return {char: s.count(char) for char in set(s)}


if __name__ == '__main__':
    cases = [
        'abcd',
        'abcabca',
        'stringsample'
    ]
    for case in cases:
        print(f'Letters count for "{case}": {count_letters(case)}')
