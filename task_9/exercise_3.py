def get_shortest_word(s: str):
    """Returns the first shortest word in the given string."""
    result = list(s)
    temp = []
    for char in s:
        if char.isspace():
            if len(temp) < len(result) and len(temp) != 0:
                result = temp[::]
            temp.clear()
        else:
            temp.append(char)
    return ''.join(result)


if __name__ == '__main__':
    cases = [
        'a \t b \t cc',
        'Shortest word in string',
        'aaa bbb cc ddd eeeee ff',
        'Usage of any split functions is forbidden.'
    ]
    for case in cases:
        print(f'String: "{case}", shortest word: "{get_shortest_word(case)}"')
