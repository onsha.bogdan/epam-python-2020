def validate_point(x: float, y: float) -> bool:
    """Check if given point in triangle with points (-1, -1), (0, 0), (1, 1)"""
    return -1 <= x <= 1 and y >= abs(x)


if __name__ == '__main__':
    x = float(input('Enter x:'))
    y = float(input('Enter y:'))

    print('Is point in shadow area:', validate_point(x, y))
