
# Task 3  
  
## Exercise 1.  
  
Write a program that determines whether the Point A(x, y) is in the shaded area or not.  
![text](https://i.ibb.co/MM7ncYK/Screenshot-2020-10-20-at-11-52-03.png)  
  
### Description  
A point will be in a shaded area if both:  
- x in the range [-1, 1]  
- y is equal or more than |x|  

[Code](./exercise_1.py)  
  
  
  
## Exercise 2.  
  
Write a program that prints the input number from 1 to 100. But for multiple of 3 print `"Fizz"` instead of the number and for multiples of 5 print `"Buzz"`.  
For numbers which are multiples of both 3 and 5 print `"FizzBuzz"`.  
  
### Description  
  
Simple if-else task, but more specific cases need to be checked earlier.  

[Code](exercise_2.py)  
  
  
  
## Exercise 3.  
  
Simulate the one round of play for baccarat game.
Rules:  
- Cards have a point value:  
  - the 2 through 9 cards in each suit are worth face value (in point.);  
  - the 10, jack, queen and king have no point value;  
  - aces are worth 1 point.  
- Sum the values of cards. If total more than 9 reduce 10 from result.  
- Player is not allower to play another cards like joker.  
  
### Description  
  
- Firstly, initialize the list of cards like the real programmers do:  
``` py  
CARDS = [*[str(i) for i in range(2, 11)], *'JQKA']  
```  
  
- Check if all of the input cards are valid:  
``` py  
if first_card not in CARDS or second_card not in CARDS:  
    return 'Do not cheat!'
 ```  
  
- Create a helping function to get card value:  
``` py  
def get_card_value(card: str) -> int:  
     return 1 if card == 'A' else 0 if card in 'JQK' else int(card)
 ```  
(No need to count card `10` as 0 points because we'll subtract 10 anyway) .
  
- Get sum of input cards value:  
``` py  
total = get_card_value(first_card) + get_card_value(second_card)  
```  
  
- Subtract 10 if the result is higher than 9, return:  
``` py  
return str(total - 10) if total > 9 else str(total)  
```  

[Code](./exercise_3.py)