def fizz_buzz(number: int):
    if not 1 <= number <= 100:
        return f'{number} is out of range [1, 100]'
    if not number % 3 and not number % 5:
        return 'FizzBuzz'
    if not number % 3:
        return 'Fizz'
    if not number % 5:
        return 'Buzz'
    return number


if __name__ == '__main__':
    while 1:
        num = int(input('Enter the number:'))

        exit(0) if num == -1 else print(fizz_buzz(num))
