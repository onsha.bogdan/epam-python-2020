CARDS = [*[str(i) for i in range(2, 11)], *'JQKA']


def play(first_card: str, second_card: str) -> str:
    """Simulate the one round of play for baccarat game."""

    def get_card_value(card: str) -> int:
        return 1 if card == 'A' else 0 if card in 'JQK' else int(card)

    if first_card not in CARDS or second_card not in CARDS:
        return 'Do not cheat!'

    total = get_card_value(first_card) + get_card_value(second_card)

    return str(total - 10) if total > 9 else str(total)


if __name__ == '__main__':
    while 1:
        first_card = input('Enter first card: ')
        second_card = input('Enter second card: ')

        if first_card == '-1' or second_card == '-1':
            exit(0)
        print('Your result:', play(first_card, second_card))
