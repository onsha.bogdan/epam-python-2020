# Task 10
  
## Exercise 1.  
Open file `data/unsorted_names.txt` in data folder. Sort the names and write them to a new file called `sorted_names.txt`. Each name should start with a new line.

### Description
Function for reading:
```py 
def read_file(filepath=os.path.join('data', 'unsorted_names.txt')):
    with open(filepath, 'r') as f:
        data = f.readlines()
    return [i.strip() for i in data if i.strip()]
```

Function for writing:

```py 
def write_file(data, filepath=os.path.join('data', 'sorted_names.txt')):
    with open(filepath, 'w') as f:
        f.write('\n'.join(data))

```

Main function:
```py 
def handle_names():
    names = read_file()
    names.sort()
    write_file(names)
```

[Code](./exercise_1.py)

## Exercise 2.  
Implement a function which search for most common words in the file.Use `data/lorem_ipsum.txt` file as an example.

### Description
- Read file using function from previous task.

```py 
from task_10.exercise_1 import read_file

lines = read_file(filepath)
```

- Convert list of lines into list of words, get rid of all non-alphabetic characters:

```py 
    def lines_to_words(lines):
        regex = re.compile('[^a-zA-Z ]')
        text = ' '.join(lines)
        text = regex.sub('', text)
        return text.split()

    lines = read_file(filepath)
    words = lines_to_words(lines)
```

- Count number of occurrences using `collections.Counter`:
```py 
occurrences = collections.Counter(words)
```

- Convert to list of names, return:
```py 
return [i[0] for i in occurrences.most_common(number_of_words)]
```

[Code](./exercise_2.py)

## Exercise 3.  
File `data/students.csv` stores information about students in CSV.

Implement a function which receives file path and returns names of top performer students.

Implement a function which receives the file path with students info and writes CSV student information to the new file in descending order of age. 

### Description

Function for reading csv file into list of dictionaries:
```py 
def read_csv(file_path):
    with open(file_path, 'r') as f:
        reader = csv.DictReader(f)
        return list(reader)
```

Top performed students:
```py 
def get_top_performers(file_path, number_of_top_students=5):
    students = read_csv(file_path)
    students.sort(key=lambda k: -float(k['average mark']))

    return [i['student name'] for i in students[0:number_of_top_students]]
```

Write to csv, age sorted:
```py 
def write_csv_age_sorted(file_path):
    students = read_csv(file_path)
    students.sort(key=lambda k: (-int(k['age']), k['student name']))

    output_path = os.path.join('data', 'sorted_students.csv')
    with open(output_path, 'w', encoding='utf8', newline='') as f:
        writer = csv.DictWriter(f, fieldnames=students[0].keys())
        writer.writeheader()
        writer.writerows(students)
```

`exercise_3.py` output:

![Task Description](https://i.ibb.co/svy6ZM6/Screenshot-2020-11-07-at-11-20-25.png)  

[Code](./exercise_3.py)