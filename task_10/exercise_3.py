import os
import csv


def read_csv(file_path):
    with open(file_path, 'r') as f:
        reader = csv.DictReader(f)
        return list(reader)


def write_csv_age_sorted(file_path):
    students = read_csv(file_path)
    students.sort(key=lambda k: (-int(k['age']), k['student name']))

    output_path = os.path.join('data', 'sorted_students.csv')
    with open(output_path, 'w', encoding='utf8', newline='') as f:
        writer = csv.DictWriter(f, fieldnames=students[0].keys())
        writer.writeheader()
        writer.writerows(students)


def get_top_performers(file_path, number_of_top_students=5):
    students = read_csv(file_path)
    students.sort(key=lambda k: -float(k['average mark']))

    return [i['student name'] for i in students[0:number_of_top_students]]


if __name__ == '__main__':
    path = os.path.join('data', 'students.csv')

    cases = [2, 3, 5, 7, 10]

    for case in cases:
        print(f'Top {case} students are: {get_top_performers(path, case)}')

    print('\nWriting students info into "data/sorted_students.csv".')
    write_csv_age_sorted(path)
