import os


def read_file(filepath=os.path.join('data', 'unsorted_names.txt')):
    with open(filepath, 'r') as f:
        data = f.readlines()
    return [i.strip() for i in data if i.strip()]


def write_file(data, filepath=os.path.join('data', 'sorted_names.txt')):
    with open(filepath, 'w') as f:
        f.write('\n'.join(data))


def handle_names():
    names = read_file()
    print('Unsorted names:', names)
    names.sort()
    print('Sorted names:', names)
    write_file(names)


if __name__ == '__main__':
    handle_names()
