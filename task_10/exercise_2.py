import re
import os
import collections

from task_10.exercise_1 import read_file


def most_common_words(filepath, number_of_words=3):
    def lines_to_words(lines):
        regex = re.compile('[^a-zA-Z ]')
        text = ' '.join(lines)
        text = regex.sub('', text)
        return text.split()

    lines = read_file(filepath)
    words = lines_to_words(lines)

    occurrences = collections.Counter(words)
    return [i[0] for i in occurrences.most_common(number_of_words)]


if __name__ == '__main__':
    path = os.path.join('data', 'lorem_ipsum.txt')

    cases = [3, 5, 7, 9]

    for case in cases:
        most_common = most_common_words(path, number_of_words=case)
        print(f'{case} most common words in "{path}": {most_common}')
