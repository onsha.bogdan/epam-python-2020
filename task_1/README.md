# Task 1
#### - Calculate the area of triangle with sides a = 4.5, b = 5.9 and c = 9
#### - Print calculated value with precision = 2

## Description
#### [Code](./task1.py)
#### Nothing to explain, Heron's formula used:
![Heron's formula](https://www.onlinemath4all.com/images/heronsformula.png)