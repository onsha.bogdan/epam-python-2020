def get_triangle_area(a, b, c):
    """Return the square of triangle with the given sides."""
    p = (a + b + c) / 2
    return (p * (p - a) * (p - b) * (p - c)) ** 0.5


if __name__ == '__main__':
    area = get_triangle_area(4.5, 5.9, 9)
    print(round(area, 2))
