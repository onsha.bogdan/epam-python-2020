class Node:
    def __init__(self, data, next=None):
        self.data = data
        self.next = next


class CustomList:
    def __init__(self, values=None):
        self.head = None

        if values:
            for value in values:
                self.append(value)

    def append(self, value):
        """Append value to the end of the list."""
        if self.head is None:
            self.head = Node(value)
            return

        current = self.head
        while current.next is not None:
            current = current.next
        current.next = Node(value)

    def insert(self, index, value):
        """Insert object before index."""
        if index > len(self):
            raise ValueError

        if index == 0:
            self.head = Node(value, self.head)
            return

        if index == len(self):
            self[-1].next = Node(value)
            return
        self[index] = value

    def clear(self):
        """Remove all items from list."""
        self.head = None

    def remove(self, value):
        """Remove first occurrence of item from list."""
        if not self.head:
            raise ValueError('list.remove(x): x not in list')

        if self.head.data == value:
            self.head = self.head.next
            return

        previous_node = self.head
        for node in self:
            if node.data == value:
                previous_node.next = node.next
                return
            previous_node = node

        raise ValueError('list.remove(x): x not in list')

    def __len__(self):

        if self.head is None:
            return 0
        count = 0
        for _ in self:
            count += 1
        return count

    def __repr__(self):
        if self.head is None:
            return 'Custom List: [None]'
        node = self.head
        nodes = []
        while node is not None:
            nodes.append(node.data)
            node = node.next

        return "CustomList: [" + " ---> ".join(
            [repr(item) for item in nodes]) + "]"

    def __getitem__(self, key):
        if isinstance(key, slice):
            start, stop, step = key.indices(len(self))

            return CustomList([self[i] for i in range(start, stop, step)])
        if isinstance(key, int):
            if key >= len(self):
                raise IndexError("list assignment index out of range")
            if key < 0:
                key = len(self) + key
            for i, item in enumerate(self):
                if i == key:
                    return item.data
            raise IndexError("list assignment index out of range")

    def __setitem__(self, index, value):
        if index >= len(self):
            raise IndexError("list assignment index out of range")

        for i, item in enumerate(self):
            if i == index:
                item.data = value
                return
        raise IndexError("list assignment index out of range")

    def __delitem__(self, index):
        self.remove(self[index])

    def __iter__(self):
        node = self.head

        while node is not None:
            yield node
            node = node.next


if __name__ == '__main__':
    lst = CustomList()
    print("List creation:")
    print("Empty: CustomList() ->", lst)
    lst = CustomList((1, 2, 3, 4, 5))
    print("Based on iterable: CustomList((1, 2, 3, 4, 5)) ->", lst, end='\n\n')

    print('Add elements:')
    lst.append('append')
    print('Append to the end: lst.append("append") ->', lst)

    lst.insert(2, 'inserted')
    print('Insert before index: lst.insert(2, "inserted") ->', lst, end='\n\n')

    print('Remove elements:')
    del lst[2]
    print('Remove by index: del lst[2] ->', lst)
    lst.remove('append')
    print('Remove by value: lst.remove("append") ->', lst, end='\n\n')

    print('Operations with elements by index:')
    print('Get value: lst[1] ->', lst[1])
    print('Get values: lst[1:3] ->', lst[1:3])
    print('Get value with negative index: lst[-2] ->', lst[-2])
    lst[0] = 10
    print('Set value: lst[0] = 10 ->', lst, end='\n\n')

    print('Get length: len(lst) ->', len(lst))
    lst.clear()
    print('Clear the list: lst.clear() ->', lst, end='\n\n')

    lst = CustomList((1, 2, 3, 4, 5, 6))
    print('Loop through the list:')
    print('lst = CustomList((1, 2, 3, 4, 5, 6))')
    print('for i in lst: print(i.data, end=" ") ->')
    for i in lst:
        print(i.data, end=' ')
    print('\n')

    print('Generating exceptions:')
    print('Get value with index out of list range: lst[100] ->')
    try:
        lst[100]
    except IndexError as e:
        print('IndexError:', str(e), end='\n\n')

    print('Remove value that is not in the list: lst.remove(42) ->')
    try:
        lst.remove(42)
    except ValueError as e:
        print('Value Error:', str(e))
