def combine_dicts(*args: dict) -> dict:
    """
    Combine several dictionaries into one. Summarize dict values in case
    of identical key. Don't change given dicts.
    Args:
        *args: dictionaries to combine.

    Returns:
        (dict): dictionary with combined values.
    """
    if not args:
        return {}

    res = args[0].copy()

    for dict_ in args[1:]:
        for key, value in dict_.items():
            if key in res:
                res[key] += value
            else:
                res[key] = value
    return res


if __name__ == '__main__':
    dict_1 = {'a': 100, 'b': 200}
    dict_2 = {'a': 200, 'c': 300}
    dict_3 = {'a': 300, 'd': 100}

    print('dict_1:', dict_1)
    print('dict_2:', dict_2)
    print('dict_3:', dict_3, end='\n\n')

    print('Combination of dict_1 and dict_2:', combine_dicts(dict_1, dict_2))
    print('Combination of dict_2 and dict_3:', combine_dicts(dict_2, dict_3))
    print('Combination of dict_1 and dict_3:', combine_dicts(dict_1, dict_3))
    print('Combination of all dicts:', combine_dicts(dict_1, dict_2, dict_3))
