# Task 6
  
## Exercise 1.  
Implement a function, that receives changeable number of dictionaries (keys - letters, values - numbers) and combines them into one dictionary.
Dict values should be summarized in case of identical keys
```py 
def combine _dicts(*args):
```

```py 
dict_1 = {'a': 100, 'b': 200}

dict_2 = {'a': 200, 'c’: 300}

dict_3 = {'a': 300, ‘d': 100}
```

```py 
print(combine dicts(dict_1, dict_2)
>>> {'a': 300, 'b’: 200, ‘c': 300}

print(combine dicts(dict_1, dict_2, dict_3)
>>> {'a': 600, 'b‘: 200, ‘c': 300, 'd’: 100}
```
### Description  
Check if there's any args:
```py 
if not args:
    return {}
```
Make a copy of the first dictionary (to not change it):
```py 
res = args[0].copy()
```
Loop through each other dictionary in args, updating values in `res`:
```py 
 for dict_ in args[1:]:
        for key, value in dict_.items():
            if key in res:
                res[key] += value
            else:
                res[key] = value
```

[Code](./exercise_1.py)

## Exercise 2.  
To create generic type CustomList ~ the list of values, which has 
length that is extended when new elements are added to the list.

Low level tasks require implementation of the following functionality:

* Creating of empty user list and the one based on elements set (the elements are stored in CustomList in form of unidirectional linked list
* Adding, removing elements

* Operations with elements by index

* Clearing the list, receiving its length

© Receiving link to linked elements list

Advanced level tasks require implementation of the following functionality:
* All completed tasks of Low level

* Generating exceptions, specified in xml-comments to class methods

* Receiving from numerator list for operator foreach

### Description  

Functional demonstration (`exercise_2.py` output)
![Task Description](https://i.ibb.co/B6prf3M/Screenshot-2020-10-29-at-11-49-54.png)  

[Code](./exercise_2.py)