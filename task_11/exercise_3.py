def call_once(func):
    result = None

    def wrapper(*args):
        nonlocal result

        if not result:
            result = func(*args)
        return result

    return wrapper


@call_once
def sum_of_numbers(a, b):
    return a + b


if __name__ == '__main__':
    cases = [
        (13, 42),
        (999, 100),
        (856, 232)
    ]
    for case in cases:
        print(f'Calling sum_of_numbers with {case}: {sum_of_numbers(*case)}\n')

