# Task 11

## Exercise 1.  
Look through file modules/legb.py.
- Find a way to call inner_function without moving it from inside of enclosed_function.
- Modify ONE LINE in inner_function to make it print variable 'a' from global scope.
- Modify ONE LINE in inner_function to make it print variable 'a' form enclosing function.

### Description
1. Calling `inner_function` from outside:
```py 
def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():
        a = "I am local variable!"
        print(a)
    return inner_function()

enclosing_funcion()
```

2.1 Make `inner_function` print variable from global scope:

```py 
def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():
        global a

        print(a)
    return inner_function()
```

2.1 Make `inner_function` print variable from enclosing function:

```py 
def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():
        nonlocal a

        print(a)
    return inner_function()
```
[Code](./modules/legb.py)


## Exercise 2.  
Implement a decorator `remember_result` which remembers last result of function 
it decorates and prints it before next call.

### Description
Decorator:
```py 
def remember_result(func):
    last_result = None

    def wrapper(*args):
        nonlocal last_result
        print("Last result -", last_result)
        last_result = func(*args)

    return wrapper
```

`exercise_2.py` output:

![File Output](https://i.ibb.co/TKr48Kw/Screenshot-2020-11-09-at-13-11-33.png)  

[Code](./exercise_2.py)

## Exercise 3.  
Implement a decorator `call_once` which runs a function or method once and 
caches the result. All consecutive calls to this function should 
return cached result no matter the arguments.

### Description
Decorator:
```py 
def call_once(func):
    result = None

    def wrapper(*args):
        nonlocal result

        if not result:
            result = func(*args)
        return result

    return wrapper

```
`exercise_3.py` output:

![File Output](https://i.ibb.co/jZt3WLQ/Screenshot-2020-11-09-at-13-13-44.png)  

[Code](./exercise_3.py)
