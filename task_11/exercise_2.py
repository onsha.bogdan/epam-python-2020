def remember_result(func):
    last_result = None

    def wrapper(*args):
        nonlocal last_result
        print("Last result -", last_result)
        last_result = func(*args)

    return wrapper


@remember_result
def sum_list(*args):
    result = ""
    for item in args:
        result += str(item)

    print(f"Current result = {result}")
    return result


if __name__ == '__main__':
    cases = [
        ("a", "b"),
        ("abc", "cde"),
        (3, 4, 5)
    ]
    for case in cases:
        print(f"Running sum_list with {case}:")
        sum_list(*case)
        print()
