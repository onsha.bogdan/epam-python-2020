from typing import Optional


def fibonacci_req(n: int, seq: Optional[int] = None) -> list:
    """Return fibonacci sequence till n-th element (recursion variant)."""
    if not seq:
        seq = [0, 1]

    if len(seq) < n:
        seq.append(seq[-1] + seq[-2])
        return fibonacci_req(n, seq)

    return seq


def fibonacci_loop(n: int) -> list:
    """Return fibonacci sequence till n-th element (loop variant)."""
    if n == 1:
        return [0]

    seq = [0, 1]
    while len(seq) < n:
        seq.append(seq[-1] + seq[-2])

    return seq


if __name__ == '__main__':
    num = int(input('Please, enter the length if sequence: '))

    sequence = fibonacci_loop(num)
    # sequence = fibonacci_req(num)

    print('Fibonacci sequence is', sequence)
    print('Sum of elements in Fibonacci sequence is', sum(sequence))
