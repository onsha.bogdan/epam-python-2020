def binary(n: int) -> str:
    """Return binary representation of the given number."""
    nums = [n]
    while n > 1:
        n = n // 2
        nums.append(n)

    bits = []
    for i in nums:
        bits.append(str(0 if i % 2 == 0 else 1))
    return ''.join(reversed(bits))


if __name__ == '__main__':
    cases = [1, 2, 22, 25, 98, 128, 278]

    for num in cases:
        num_binary = binary(num)

        print(f'Number is {num}, binary representation is {num_binary}, '
              f'sum is {num_binary.count("1")}')
