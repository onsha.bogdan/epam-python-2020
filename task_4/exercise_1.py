from typing import List


def print_matrix(matrix: List[List[int]]) -> None:
    for row in matrix:
        print(row)
    print()


def transpose(matrix: List[List[int]]) -> List[List[int]]:
    """Return transposed matrix from the given one."""
    return list(map(list, zip(*matrix)))


if __name__ == '__main__':
    matrix = [
        [0, 1, 9],
        [9, 8, 5],
        [7, 3, 1],
        [3, 4, 0],
    ]
    print('Original matrix is:')
    print_matrix(matrix)

    print('Transposed matrix is:')
    print_matrix(transpose(matrix))
