def factorial(n):
    """Return factorial of the given number."""
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result


if __name__ == '__main__':
    while True:
        num = int(input('Please, enter the number:'))

        if num == -1:
            break
        print(f'Factorial of {num} is {factorial(num)}\n')
