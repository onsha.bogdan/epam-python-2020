# Task 4
  
## Exercise 1.  
  
### Transpose a matrix 
![text](https://i.ibb.co/PjkXWFh/Screenshot-2020-10-22-at-14-03-47.png)  
  
### Description  
Matrix transposition function looks like:
``` py  
def transpose(matrix=):
    return list(map(list, zip(*matrix)))
```  
Main steps are:

1. Pass list of lists in `zip` as unpacked arguments list;
2. Wrap that in `map` function, pass `list` as a first argument to get an iterable;
3. Convert the result back to `list`.

[Code](./exercise_1.py)  


## Exercise 2.  
  
### Factorial 
![Task Description](https://i.ibb.co/CV5cZ7V/Screenshot-2020-10-22-at-14-14-06.png)  
  
### Description  
Function to count factorial:
```py 
def factorial(n):
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result
```

[Code](./exercise_2.py)  

## Exercise 3.  
  
### Fibonacci sequence
![Task Description](https://i.ibb.co/RvCs6mg/Screenshot-2020-10-22-at-14-28-51.png)  
  
### Description  
Variant with loop:
```py 
def fibonacci(n):
    if n == 1:
        return [0]

    seq = [0, 1]
    while len(seq) < n:
        seq.append(seq[-1] + seq[-2])

    return seq
```

Variant with recursion:
```py 
def fibonacci_req(n, seq):
    if not seq:
        seq = [0, 1]

    if len(seq) < n:
        seq.append(seq[-1] + seq[-2])
        return fibonacci_req(n, seq)

    return seq
```

[Code](./exercise_3.py)  

## Exercise 4.  
  
### Binary representation
![Task Description](https://i.ibb.co/sWK1dXZ/Screenshot-2020-10-22-at-14-31-35.png)  
  
### Description  
Converting decimal to binary by dividing by 2.
```py 
def binary(n):
    nums = [n]
    while n > 1:
        n = n // 2
        nums.append(n)

    bits = []
    for i in nums:
        bits.append(str(0 if i % 2 == 0 else 1))
    return ''.join(reversed(bits))
```

[Code](./exercise_4.py)  