# Task 12

## Exercise 1. 
- Invoke `pwd` to see your current working directory (there should be your home
 directory).
 
![pwd output](screenshots/1a_1.png)  

- Collect output of these commands:
    - `ls -l /` - list root directory content in long listing format.
    ![ls-l / output](screenshots/1b_1.png)  
    - `ls` - list current directory in default format.
    ![ls output](screenshots/1b_2.png)  
    - `ls ~`  - list home directory.
    ![ls ~ output](screenshots/1b_3.png)  
    - `ls -l` - list current directory in long listing format.
    ![ls -l output](screenshots/1b_4.png) 
    - `ls -a` - list current directory, including entries starting with `.`.
    ![ls -a output](screenshots/1b_5.png) 
    - `ls -la` - list all entries in current directory in long listing format.
    ![ls -la output](screenshots/1b_6.png) 
    - `ls -lda ~` - list all directories in long listing format in home directory.
    ![ls -lda ~ output](screenshots/1b_7.png) 
    
- Execute and describe the following commands (store the output, if any):
    - `mkdir test` - create directory "test".
    - `cd test` - change current directory to `test`.
    - `pwd` - list current path.
    ![pwd](screenshots/1c_1.png) 
    - `touch test.txt` - create file `test.txt`.
    - `ls -l test.txt` - list info about `test.txt` in long listing format.
    ![ls -l test.txt ](screenshots/1c_2.png) 
    - `mkdir test2` - create directory "test2".
    - `mv test.txt test2` - move file `test.txt` to `test2` directory.
    - `cd test2` - change current directory to `test2`.
    - `ls` - list entries in current directory.
    ![ls](screenshots/1c_3.png) 
    - `mv test.txt test2.txt` - rename file `test.txt` to `test2.txt`.
    - `ls` - list entries in current directory.
    ![ls](screenshots/1c_4.png) 
    - `cp test2.txt ..` - copy `test2.txt` into parent folder.
    - `cd ..`- change current directory to parent folder.
    - `ls` - list entries in current directory.
    ![ls](screenshots/1c_5.png) 
    - `rm test2.txt` - delete `test2.txt` file.
    - `rmdir test2` - delete `test2` directory (if empty).
    ![rmdir test2](screenshots/1c_6.png) 
    
   
- Execute and describe the difference:
    - `cat /etc/fstab` - string manipulation program, which allows to read file sequentially, writing it to the standard output.
    - `less /etc/fstab` - file reading program, that reads a file one screen at a time, and loads more of the file as you scroll through it.
    - `more /etc/fstab` - file reading program, filter for paging through text one screenful at a time.
    

## Exercise 2. 
- Discovering soft and hard links. Comment on results of these commands 
(place the output into your report):
    - `cd`
    - `mkdir test`
    - `cd test`
    - `touch test1.txt`
    - `echo “test1.txt” > test1.txt`
    - `ls -l .` (a hard link)
    ![ls -l .](screenshots/2a_1.png) 
    - `ln test1.txt test2.txt`
    - `ls -l .` // (payattentionto the number of links to test1.txt and test2.txt)
    ![ls -l .](screenshots/2a_2.png) 
    - `echo “test2.txt” > test2.txt`
    - `cat test1.txt test2.txt`
    ![cat test1.txt test2.txt](screenshots/2a_3.png) 
    
    - `rm test1.txt`
    - `ls -l .` // (now a softlink)
    ![ls-l .](screenshots/2a_4.png) 
    
    - `ln -s test2.txt test3.txt`
    - `ls -l .` //(payattentionto the number of links to the created files)
    ![ls-l .](screenshots/2a_5.png) 
    - `rm test2.txt;ls -l .`
    ![rm test2.txt;ls -l .](screenshots/2a_6.png) 
    
    
- Dealing with `chmod`.

    An executable script. Open your favorite editor and put these lines into a file:
    ```sh 
    #!/bin/bash
    echo “Drugs are bad MKAY?”
    ```
    Give name “script.sh” to the script and call to `chmod+x script.sh`.
    Then you are ready to execute the script: `./script.sh`.
    
    Output:
    ![./script.sh](screenshots/2b_1.png) 
