# Task 7
  
## Exercise 1.  
Develop Rectangle and ArrayRectangles with a predefined functionality.

On a Low level it is obligatory:
To develop Rectangle class with following content:

- 2 closed float sideA and sideB (sides A and B of the rectangle)
- Constructor with two parameters a and b (parameters specify rectangle sides)
- Constructor with a parameter a (parameter specify side A of a rectangle, side B is always equal to 5)
- Method GetSideA, returning value of the side A
- Method GetSideB, returning value of the side B
- Method Area, calculating and returning the area value
- Method Perimeter, calculating and returning the perimeter value
- Method IsSquare, checking whether current rectangle is shape square or not. Returns true if the shape is square and false in another case.
- Method ReplaceSides, swapping rectangle sides

On Advanced level also needed:

- Complete Level Low Assignment
- Develop class ArrayRectangles, in which declare:
- Private field rectangle_array - array of rectangles
- Constructor creating an empty array of rectangles with length n
- Constructor that receives an arbitrary amount of objects of type Rectangle or an array of objects of type Rectangle.
- Method AddRectangle that adds a rectangle of type Rectangle to the array on the nearest free place and returning true, or returning false, if there is
no free space in the array
- Method NumberMaxArea, that returns order number (index) of the rectangle with the maximum area value (numeration starts from zero)

### Description
Functional demonstration (`exercise_1.py` output)
![Task Description](https://i.ibb.co/n6fcx6M/Screenshot-2020-10-30-at-17-28-37.png)  
[Code](./exercise_1.py)

## Exercise 2.


To create classes Employee, SalesPerson, Manager and Company with predefined functionality.

### Low level requires:

To create basic class Employee and declare following content:
- Three closed fields — text field name (employee last name), money fields — salary and bonus
- Public property Name for reading employee’s last name
- Public property Salary for reading and recording salary field
- Constructor with parameters string name and money salary (last name and salary are set)
- Method SetBonus that sets bonuses to salary, amount of which is delegated/conveyed as bonus
- Method ToPay that returns the value of summarized salary and bonus.

To create class SalesPerson as class Employee inheritor and declare within it:
- Closed integer field percent (percent of sales targets plan performance/execution)
- Constructor with parameters: name — employee last name, salary, percent — percent of plan performance, first two of which are passed to basic class
constructor
- Redefine method of parent class SetBonus in the following way: if the sales person completed the plan more than 100%, so his bonus is doubled (is
multiplied by 2), and if more than 200% - bonus is tripled (is multiplied by 3)  

To create class Manager as Employee class inheritor, and declare with it:
- Closed integer field quantity (number of clients, who were served by the manager during a month)
- Constructor with parameters string name — employee last name, salary and integer cllentAmount — number of served clients, first two of which are
passed to basic class constructor.
- Redefine method of parent class SetBonus in the following way: if the manager served over 100 clients, his bonus is increased by 500, and if more
than 150 clients — by 1000.


Advanced level requires:

- To fully complete Low level tasks.
- Create class Company and declare within it:
- Constructor that receives employee array of Employee type with arbitrary length
- Method GiveEverbodyBonus with money parameter companyBonus that sets the amount of basic bonus for each employee.
- Method TotafToPay that returns total amount of salary of all employees including awarded bonus
- Method NameMaSalary that returns employee last name, who received maximum salary including bonus.

#### Description
Functional demonstration (`exercise_2.py` output)
![Task Description](https://i.ibb.co/tQmwHgy/Screenshot-2020-10-30-at-17-35-23.png)  
[Code](./exercise_2.py)