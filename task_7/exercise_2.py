class Employee:
    _name: str
    _salary: float
    _bonus: float

    def __init__(self, name, salary):
        self._name = name
        self.salary = salary
        self._bonus = 0.0

    @property
    def name(self):
        return self._name

    @property
    def salary(self):
        return self._salary

    @salary.setter
    def salary(self, value):
        self._salary = float(value)

    @property
    def bonus(self):
        return self._bonus

    @bonus.setter
    def bonus(self, value):
        self._bonus = float(value)

    def to_pay(self):
        return self.salary + self.bonus

    def __repr__(self):
        return f"name = {self._name}, salary = {self.salary}, " \
               f"bonus = {self.bonus};" \
               f" {self.__class__.__name__} class"


class SalesPerson(Employee):
    _percent: int

    def __init__(self, name, salary, percent):
        self.percent = percent
        super().__init__(name, salary)

    @property
    def percent(self):
        return self._percent

    @percent.setter
    def percent(self, value):
        self._percent = int(value)

    @property
    def bonus(self):
        return self._bonus

    @bonus.setter
    def bonus(self, value):

        if self.percent <= 100:
            self._bonus = float(value)
        elif 100 < self.percent <= 200:
            self._bonus = float(2 * value)
        else:
            self._bonus = float(3 * value)


class Manager(Employee):
    _quantity: int

    def __init__(self, name, salary, client_amount):
        self.client_amount = client_amount
        super().__init__(name, salary)

    @property
    def client_amount(self):
        return self._quantity

    @client_amount.setter
    def client_amount(self, value):
        self._quantity = int(value)

    @property
    def bonus(self):
        return self._bonus

    @bonus.setter
    def bonus(self, value):
        if 100 < self.client_amount <= 150:
            self._bonus = float(value + 500)
        elif self.client_amount > 150:
            self._bonus = float(value + 1000)


class Company:
    _employees: list

    def __init__(self, *args):
        self._employees = []

        for emp in args:
            if isinstance(emp, Employee):
                self._employees.append(emp)

    def give_everyone_bonus(self, companyBonus):
        for emp in self._employees:
            emp.bonus = companyBonus

    def name_max_salary(self):
        max_salary = max([emp.to_pay() for emp in self._employees])

        for emp in self._employees:
            if emp.to_pay() == max_salary:
                return emp.name

    def __repr__(self):
        repr_ = []
        for i in self._employees:
            repr_.append(repr(i))
        return f'Company: [{";".join(repr_)}]'


if __name__ == '__main__':
    print('Low level:', end='\n\n')
    print('Employee: ')
    emp1 = Employee('Bezos', 120)
    print('Create instance: Employee("Bezos", 120) -> ', emp1, end='\n\n')
    print('Read name: emp1.name ->', emp1.name)
    print('Read salary: emp1.salary ->', emp1.salary)

    emp1.salary = 140
    print('Write salary: emp1.salary=140 ->', emp1.salary, end='\n\n')

    emp1.bonus = 100
    print('Set bonus: emp1.bonus=100 ->', emp1.bonus)
    print('To pay: emp.to_pay() ->', emp1.to_pay(), end='\n\n\n')

    print('SalesPerson:')
    emp2 = SalesPerson('Gates', 1200, 110)
    print('Create instance: SalesPerson("Gates", 1200, 110) -> ', emp2,
          end='\n\n')
    emp2.bonus = 200
    print('Set bonus: emp.bonus=200 ->', emp2)
    print('To pay: emp.to_pay() ->', emp2.to_pay(), end='\n\n\n')

    print('Manager:')
    emp3 = Manager('Musk', 1200, 110)
    print('Create instance: Manager("Musk", 1200, 110) -> ', emp3,
          end='\n\n')
    emp3.bonus = 200
    print('Set bonus: emp.bonus=200 ->', emp3)
    print('To pay: emp.to_pay() ->', emp3.to_pay(), end='\n\n\n')

    print('Advanced:', end='\n\n')
    print('Company:')
    emp1 = Employee('Bezos', 120)
    emp2 = SalesPerson('Gates', 1200, 110)
    emp3 = Manager('Musk', 1200, 110)
    company = Company(emp1, emp2, emp3)
    print('Create company with all of the previous employees: '
          'Company(emp1, emp2, emp3) ->')
    print(company, end='\n\n')

    company.give_everyone_bonus(100)
    print('Give everyone bonus: company.give_everyone_bonus(100) ->')
    print(company)

    print('Employee with max salary: company.name_max_salary() ->',
          company.name_max_salary())
