class Rectangle:
    def __init__(self, a, b=5):
        self.__a = float(a)
        self.__b = float(b)

    @property
    def a(self):
        return self.__a

    @a.setter
    def a(self, value):
        self.__a = float(value)

    @property
    def b(self):
        return self.__b

    @b.setter
    def b(self, value):
        self.__b = float(value)

    def area(self):
        return self.a * self.b

    def perimeter(self):
        return 2 * (self.a + self.b)

    def is_square(self):
        return self.a == self.b

    def replace_sides(self):
        self.a, self.b = self.b, self.a

    def __repr__(self):
        return f'Rectangle [{self.a}, {self.b}]'


class ArrayRectangles:
    def __init__(self, *args):
        self.__rectangles = []
        if args:
            for rectangle in args:
                self.__rectangles.append(rectangle)

    def add_rectangle(self, rectangle: Rectangle):
        self.__rectangles.append(rectangle)

    def max_area(self):
        areas = [rect.area() for rect in self.__rectangles]
        return areas.index(max(areas))

    def min_perimeter(self):
        perimeters = [rect.perimeter() for rect in self.__rectangles]
        return perimeters.index(min(perimeters))

    def squares_count(self):
        return [rect.is_square() for rect in self.__rectangles].count(True)

    def __repr__(self):
        repr_ = ', '.join([str(rect) for rect in self.__rectangles])

        return f"ArrayRectangles: [{repr_}]"


if __name__ == '__main__':
    print('Low level:', end='\n\n')
    print('Constructors:')
    print('With one parameter: Rectangle(10) ->', Rectangle(10))
    print('With two parameters: Rectangle(10, 15) ->', Rectangle(10, 15))

    r = Rectangle(10, 15)
    print('\nGet/Set sides:')
    print('Get side A: r.a ->', r.a)
    r.a = 20
    print('Set side A: r.a = 20 ->', r, end='\n\n')

    print('Get side B: r.b ->', r.b)
    r.b = 25
    print('Set side B: r.b = 25 ->', r, end='\n\n')

    print('Other methods:')
    print('Get area: r.area() ->', r.area())
    print('Get perimeter: r.perimeter() ->', r.perimeter())
    print('Is square: r.is_square() ->', r.is_square())
    r.replace_sides()
    print('Replace sides: r.replace_sides() ->', r, end='\n\n')

    print('Advanced:', end='\n\n')
    print('Constructors:')
    print('Empty: ArrayRectangles() ->', ArrayRectangles())
    arr = ArrayRectangles(Rectangle(5, 5), Rectangle(10, 7), Rectangle(5, 8))
    print('With the given rectangles: ArrayRectangles('
          'Rectangle(5, 5), Rectangle(10, 7), Rectangle(5, 8)) ->')
    print(arr, end='\n\n')

    arr.add_rectangle(Rectangle(2, 4))
    print('Add rectangle: arr.add_rectangle(Rectangle(2, 4)) ->')
    print(arr, end='\n\n')

    print(
        'Index of rectangle with maximum area: arr.number_max_area() ->',
        arr.max_area()
    )
    print(
        'Index of rectangle with minimum perimeter: arr.number_min_perimeter() ->',
        arr.min_perimeter()
    )
    print(
        'Number of squares in array: arr.squares_count() ->',
        arr.squares_count()
    )
