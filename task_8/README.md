# Task 8
  
## Exercise 1.  
![Task Description](https://i.ibb.co/sbh1GK4/Screenshot-2020-11-01-at-13-55-41.png)  

Input --> output cases:
- not integer --> ValueError
- integer in range (-∞, 7] --> AssertionError "not enough"
- integer in range (7, +∞) --> None

## Exercise 2.  
![Task Description](https://i.ibb.co/qrcL680/Screenshot-2020-11-01-at-13-56-51.png)  
Cases:
1. `x` is defined upper, `bar` is defined and won't raise any exceptions --> both strings will be printed.
2. `x` or `bar` is not defined --> `NameError` exception will be raised, `"after bar"` will be printed. 

## Exercise 3.  
![Task Description](https://i.ibb.co/0qc957J/Screenshot-2020-11-01-at-13-57-26.png)  
Output --> 2, `finally` block is executed finally regardless or exception.

## Exercise 4.  
![Task Description](https://i.ibb.co/9VJNQw6/Screenshot-2020-11-01-at-13-57-56.png)  
Output --> `SyntaxError`, `else` block can be used only after `except`.

## Exercise 5.  
![Task Description](https://i.ibb.co/H4SMVPV/Screenshot-2020-11-01-at-13-58-22.png)  
Two identical exceptions will be raised (`TypeError`?), because `"Error"` must be an inheritor of `BaseException`:
- in `raise "Error"`
- in `except "Error"`

## Exercise 6.  
![Task Description](https://i.ibb.co/0FvfsMX/Screenshot-2020-11-01-at-13-58-52.png)  
Infinity loop. If filename, entered by user is valid - nothing will happen.
Otherwise - `"Input file not found"` will be printed.