# Task 5
  
## Exercise 1.  
Create function `is_sorted`, determining whether a given list of integer 
values of arbitrary length is sorted in a given order (the order is set up 
by enum value `SortOrder`). Array and sort order are passed by parameters. 
Function does not change the array.

  
### Description  
Define SortOrder this way:
```py 
from enum import Enum

class SortOrder(Enum):
    ASCENDING = 0
    DESCENDING = 1
```
So we can pass sort order value as a `reverse` parameter in `sorted()`
while checking if the given list is sorted:
```py 
def is_sorted(lst: List[int], order: SortOrder) -> bool:
    return lst == sorted(lst, reverse=order.value)
```

[Code](./exercise_1.py)


## Exercise 2.  
Create function `transform`, replacing the value of each element of an integer 
array with the sum of this element value and its index, only if the given 
array is sorted in the given order (the order is set up by enum value 
`SortOrder`). Array and sort order are passed by parameters. To check, if the 
array is sorted, the function IsSorted from the Task 1 is called.

  
### Description  
Check if the list is sorted using the `is_sorted` 
function from task 1. If so, add indexes.
```py 
def transform(lst, order):
    if not is_sorted(lst, order):
        return lst

    for index, item in enumerate(lst):
        lst[index] += index
    return lst

```


[Code](./exercise_2.py)



## Exercise 3.  
Create function `mult_arithmetic_elements`, which determines the multiplication 
of the first `n` elements of arithmetic progression of real numbers with a given 
initial element of progression `a1` and progression step `t`.

  
### Description  
Create generator for handling arithmetic progression:
```py 
def arithmetic_progression(start, step, count):
    for _ in range(count):
        yield start
        start += step
```
Use it in `mult_arithmetic_elements` function:
```py 
def mult_arithmetic_elements(start, step, count):
    result = 1

    for i in arithmetic_progression(start, step, count):
        result *= i

    return result
```
[Code](./exercise_3.py)

## Exercise 4.  
Create function `sum_geometric_elements`, determining the sum of the first 
elements of a decreasing geometric progression of real numbers with a given 
initial element of a progression `a1` and a given progression step `t`, while 
the last element must be greater than a given `a_lim`.


  
### Description  
Create generator for handling geometric progression:
```py 
def geometric_progression(start, step, limit):
    while start > limit:
        yield round(start, 2)
        start *= step

```
Use it in `sum_geometric_elements` function:
```py 
def sum_geometric_elements(start, stop, limit):
    return int(sum([i for i in geometric_progression(start, stop, limit)]))
```
[Code](./exercise_4.py)