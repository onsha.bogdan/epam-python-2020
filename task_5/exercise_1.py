from enum import Enum
from typing import List


class SortOrder(Enum):
    ASCENDING = 0
    DESCENDING = 1


def is_sorted(lst: List[int], order: SortOrder) -> bool:
    """
    Check whether the given list is sorted in the given order.
    Don't change the given list.
    """
    return lst == sorted(lst, reverse=order.value)


if __name__ == '__main__':
    cases = [
        ([1, 2, 3, 4, 5], SortOrder.ASCENDING),
        ([5, 4, 3, 2, 1], SortOrder.DESCENDING),
        ([3, 1, 2, 5, 4], SortOrder.ASCENDING),
        ([3, 1, 2, 5, 4], SortOrder.DESCENDING),
    ]
    for case in cases:
        print(f'List {case[0]} is sorted in {case[1]}: {is_sorted(*case)}')
