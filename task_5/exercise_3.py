def arithmetic_progression(start, step, count):
    """Yields next element in the given arithmetic progression."""
    for _ in range(count):
        yield start
        start += step


def mult_arithmetic_elements(start, step, count):
    """
    Return multiplication of first {count} elements of the given arithmetic
    progression.
    Args:
        start (int): initial value.
        step (int): progression step value.
        count (int): count of elements to generate.
    """
    result = 1

    for i in arithmetic_progression(start, step, count):
        result *= i

    return result


if __name__ == '__main__':
    cases = [
        (5, 3, 4),
        (10, 10, 3),
        (5, 5, 5)
    ]

    for case in cases:
        print(
            f'For a1={case[0]}, t={case[1]}, n={case[2]}, '
            f'multiplication equals to '
            f'{"*".join([str(i) for i in arithmetic_progression(*case)])} '
            f'= {mult_arithmetic_elements(*case)}'
        )
