from typing import List

from task_5.exercise_1 import SortOrder, is_sorted


def transform(lst: List[int], order: SortOrder) -> List[int]:
    """
    Replace each value in the given list of integers with the sum of element
    value and it's index if the given list is sorted in the given order.
    """
    if not is_sorted(lst, order):
        return lst

    for index, item in enumerate(lst):
        lst[index] += index
    return lst


if __name__ == '__main__':
    cases = [
        ([5, 17, 24, 88, 33, 2], SortOrder.ASCENDING),
        ([15, 10, 3], SortOrder.ASCENDING),
        ([15, 10, 3], SortOrder.DESCENDING),
    ]
    for case in cases:
        print(
            f'List: {case[0]}\n'
            f'Sort order: {case[1]}\n'
            f'List values after call transform(): {transform(*case)}\n'
        )
