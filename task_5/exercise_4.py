def geometric_progression(start, step, limit):
    """Yields next element in the given geometric progression."""
    while start > limit:
        yield round(start, 2)
        start *= step


def sum_geometric_elements(start: float, stop: float, limit: float):
    """
      Return sum of the first elements of a decreasing geometric progression,
      while the last element is greater than {limit}.

      Args:
          start (float): initial element of progression.
          stop (float): progression step value.
          limit (float): limit value to handle.
      Returns:
          int: rounded sum of the resulted sequence.
      """
    return int(sum([i for i in geometric_progression(start, stop, limit)]))


if __name__ == '__main__':
    cases = [
        (100, 0.5, 20),
        (1000, 0.75, 500),
        (10, 0.75, 1)
    ]

    for case in cases:
        print(
            f'For a1={case[0]}, t={case[1]}, n={case[2]}, '
            f'multiplication equals to '
            f'{" + ".join([str(i) for i in geometric_progression(*case)])} '
            f'≈ {sum_geometric_elements(*case)}'
        )
