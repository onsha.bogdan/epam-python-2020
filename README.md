# Bohdan Onsha's homework for EPAM Python 2020

- ### [Task 1](./task_1/)
- ### [Task 2](./task_2/)
- ### [Task 3](./task_3/)
- ### [Task 4](./task_4/)
- ### [Task 5](./task_5/)
- ### [Task 6](./task_6/)
- ### [Task 7](./task_7/)
- ### [Task 8](./task_8/)
- ### [Task 9](./task_9/)
- ### [Task 10](./task_10/)
- ### [Task 11](./task_11/)
- ### [Task 12](./task_12/)