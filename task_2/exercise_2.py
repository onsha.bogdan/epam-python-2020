def change_position(lst: list):
    """Swap first and last elements in list."""
    lst[0], lst[-1] = lst[-1], lst[0]
    return lst


if __name__ == '__main__':
    players = ['Ashleigh Barty', 'Simona Halep', 'Naomi Osaka',
               'Karolina Pliskova', 'Elina Svitolina']

    print('Original list:', players)
    print('Modified list:', change_position(players))
