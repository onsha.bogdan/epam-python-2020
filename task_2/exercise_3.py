def swap_words(quote, w1, w2):
    """Swap two substrings in string."""
    words = [w1 if x == w2 else w2 if x == w1 else x for x in quote.split()]

    return ' '.join(words)


if __name__ == '__main__':
    quote = "The reasonable man adapts himself to the world: the " \
            "unreasonable one persists in trying to adapt the world to " \
            "himself. Therefore all progress depends on the unreasonable man. "

    print('Original quote:', quote)
    print('Modified quote:', swap_words(quote, 'reasonable', 'unreasonable'))
