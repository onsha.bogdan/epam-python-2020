# Task 2

## Exercise 1.

#### - Return the count of negative numbers in next list [4, -9, 8, -11, 8]
#### - Note: do not use conditionals or loops

### Description
#### `filter()` function has used to get rid of all non-negative numbers. 
#### [Code](./exercise_1.py)



## Exercise 2.

#### - You have first 5 best players according APT rankings. Set the first-place player (at the front of the list) to last place and vise versa
#### - players = ['Ashleigh Barty', 'Simona Halep', 'Naomi Osaka', 'Karolina Pliskova', 'Elina Svitolina']

### Description

#### Standard pythonic way to swap two variables (`a, b = b, a`) used. 
#### [Code](./exercise_2.py)



## Exercise 3.

#### - Swap words `reasonable` and `unreasonable` in quote `The reasonable man adapts himself to the world: the unreasonable one persists in trying to adapt the world to himself. Therefore all progress depends on the unreasonable man.`
#### - Note. Do not use `<string>.replace()` function or similar.

### Description

#### 1. Divide quote into list of words with `split()`.
#### 2. Replace each occurrence of `reasonable` and `unreasonable` with the opposite word.
#### 3. Convert list of words back to string with `' '.join(words)`
#### [Code](./exercise_3.py)