def count_negatives(numbers: list) -> int:
    """Return number of negative numbers in list."""
    return len(list(filter(lambda x: x < 0, numbers)))


if __name__ == '__main__':
    lst = [4, -9, 8, -11, 8]
    print(f'There are {count_negatives(lst)} negative numbers in list {lst}')
